let tsetFunc = (arg1) => {
    console.log(arg1);
};
tsetFunc('hahaha');
class TestClass {
    constructor(pVal02 = 'インスタンス化時のデフォルト値') {
        this.pVal02 = pVal02;
        this.pVal01 = '値１';
        console.log(`インスタンス化時に渡された値は${pVal02}です。`);
    }
    get getPVal01() {
        return this.pVal01;
    }
    ;
    set setPVal01(arg01) {
        this.pVal01 = arg01;
    }
    get getPVal02() {
        return this.pVal02;
    }
    ;
}
let testInst = new TestClass('外からの値');
console.log(`ゲッターで取得した値は${testInst.getPVal01}です。`);
testInst.setPVal01 = '変更後の値';
console.log(`セッターで変更した値は${testInst.getPVal01}です。`);
console.log(`ゲッターで取得した２つ目の値は${testInst.getPVal02}です。`);
// 配列
let testArray = ['1', '2', '3'];
console.log('--- testArray ---');
console.log(testArray);
// 多次元配列
let testMdArray = [['1-1', '1-2', '1-3'], ['2-1', '2-2', '2-3'], ['3-1', '3-2', '3-3']];
console.log('--- testMdArray ---');
console.log(testMdArray);
// 連想配列（ハッシュ）
let testHash01 = {
    key1: 'val1',
    key2: 'val2',
    key3: 'val3',
};
console.log('--- testHash01 ---');
console.log(testHash01);
let testHash02 = {
    1: 'val1',
    2: 'val2',
    3: 'val3',
};
console.log('--- testHash02 ---');
console.log(testHash02);
let testHash03 = {
    1: 'val1',
    2: 'val2',
    3: 'val3',
};
console.log('--- testHash03 ---');
console.log(testHash03);
let testObj01 = {
    key1: 1,
    key2: 2,
    key3: 'objVal3',
};
console.log('--- testObj01 ---');
console.log(testObj01);
let testObj02 = {
    key1: 'iFval01',
    key2: 'iFval02',
    key3: 3
};
console.log('--- testObj02 ---');
console.log(testObj02);
// 列挙型
var testEnum;
(function (testEnum) {
    testEnum[testEnum["enum1"] = 0] = "enum1";
    testEnum[testEnum["enum2"] = 2] = "enum2";
    testEnum[testEnum["enum3"] = 3] = "enum3";
})(testEnum || (testEnum = {}));
console.log('--- testEnum ---');
console.log(testEnum);
console.log(testEnum.enum3);
console.log(testEnum[2]);
