let tsetFunc = (arg1: string) => {
    console.log(arg1);
}

tsetFunc('hahaha');

class TestClass {
    protected pVal01: string = '値１';
    constructor(protected pVal02: string = 'インスタンス化時のデフォルト値') {
        console.log(`インスタンス化時に渡された値は${pVal02}です。`);
    }

    get getPVal01(): string{
        return this.pVal01;
    };
    
    set setPVal01(arg01: string){
       this.pVal01 = arg01;
    }

    get getPVal02(): string{
        return this.pVal02;
    };
}

let testInst = new TestClass('外からの値');

console.log(`ゲッターで取得した値は${testInst.getPVal01}です。`);

testInst.setPVal01 = '変更後の値';

console.log(`セッターで変更した値は${testInst.getPVal01}です。`);

console.log(`ゲッターで取得した２つ目の値は${testInst.getPVal02}です。`);

// 配列 「new Array() での定義非推奨」new Array(数値) での定義は要素数となり混乱するため
let testArray:string[] = ['1', '2', '3'];
console.log('--- testArray ---');
console.log(testArray);

// 多次元配列
let testMdArray:string[][] = [['1-1', '1-2', '1-3'], ['2-1', '2-2', '2-3'], ['3-1', '3-2', '3-3']];
console.log('--- testMdArray ---');
console.log(testMdArray);

// 連想配列（ハッシュ）
let testHash01: {[key: string]: string} = {
    key1: 'val1',
    key2: 'val2',
    key3: 'val3',
}
console.log('--- testHash01 ---');
console.log(testHash01);

let testHash02: {[key: number]: string} = {
    1: 'val1',
    2: 'val2',
    3: 'val3',
}
console.log('--- testHash02 ---');
console.log(testHash02);

let testHash03: {[key: string]: string} = {
    1: 'val1',
    2: 'val2',
    3: 'val3',
}
console.log('--- testHash03 ---');
console.log(testHash03);

// オブジェクト
interface testIF01 {
    [key: string]: number | string;
}
let testObj01: testIF01 = {
    key1: 1,
    key2: 2,
    key3: 'objVal3',
}
console.log('--- testObj01 ---');
console.log(testObj01);

interface testIF02 {
    key1: string,
    key2: string,
    key3: number
}
let testObj02: testIF02 = {
    key1: 'iFval01',
    key2: 'iFval02',
    key3: 3
}
console.log('--- testObj02 ---');
console.log(testObj02);

// 列挙型
enum testEnum {
    enum1, // 自動で値0が割り振られる
    enum2 = 2, // 「列挙子 = 値」とすることで自動で割り振られる値を変更できる
    enum3, // 値は３となる
}
console.log('--- testEnum ---');
console.log(testEnum);
console.log(testEnum.enum3);
console.log(testEnum[2]);

// タプル型は型が混在して紛らわしいのでオブジェクトを使う